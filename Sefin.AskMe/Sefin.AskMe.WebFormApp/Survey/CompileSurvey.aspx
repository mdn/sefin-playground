﻿<%@ Page Title="Compile survey" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CompileSurvey.aspx.cs" Inherits="Sefin.AskMe.WebFormApp.Survey.CompileSurvey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="title-survey">
        <asp:Label runat="server" ID="LblTitle"></asp:Label></h1>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
                <label>Name</label>
                <asp:TextBox runat="server" ID="TxtName" CssClass="form-control" Text='<%# Bind("Name") %>' />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="TxtName" ErrorMessage="Required field" />
            </div>

            <div class="form-group">
                <label><asp:Label runat="server" ID="LblQuestion1" ></asp:Label></label>
                <asp:RadioButtonList runat="server" ID="RdlAnswer1" />
            </div>
            
            <asp:Panel runat="server" ID="PnlQuestion2" Visible="False" CssClass="form-group">
                <label><asp:Label runat="server" ID="LblQuestion2"></asp:Label></label>
                <div >
                    <asp:RadioButtonList runat="server" ID="RdlAnswer2" />       

                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="PnlQuestion3" Visible="False" CssClass="form-group">
                <label><asp:Label runat="server" ID="LblQuestion3"></asp:Label></label>
                <div >
                    <asp:RadioButtonList runat="server" ID="RdlAnswer3" />
                </div>
            </asp:Panel>
            
        </div>
        <div class="panel-footer">
            <asp:LinkButton runat="server" ID="LnkSave" CssClass="btn btn-primary" 
                OnClick="LnkSave_OnClick" OnClientClick="return confirm('Do you want to proceed?');"
                 CausesValidation="true">
                <span class="glyphicon glyphicon-ok"></span> Confirm
            </asp:LinkButton>

            <asp:HyperLink runat="server" ID="LinkGoBack" NavigateUrl="~/Survey/" CssClass="btn btn-default">
                <span class="glyphicon glyphicon-circle-arrow-left"></span> Back to the survey list
            </asp:HyperLink>
        </div>
    </div>
</asp:Content>
