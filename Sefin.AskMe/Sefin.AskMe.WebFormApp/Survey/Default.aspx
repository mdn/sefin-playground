﻿<%@ Page Title="Survey" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" MasterPageFile="../Site.Master" Inherits="Sefin.AskMe.WebFormApp.Survey.Default" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="title-survey">Select a survey</h1>
    
    
    <div class="panel panel-default">
        <div class="panel-body">
            <asp:TextBox runat="server" ID="TxtSearch" CssClass="form-control"  placeholder="Search text"/>
        </div>
        <div class="panel-footer">
            <asp:LinkButton CssClass="btn btn-primary" runat="server" Id="BtnSearch" OnClick="BtnSearch_Click">
                <span class="glyphicon glyphicon-search"></span> Search
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-default" runat="server" Id="BtnCancelSearch" OnClick="BtnCancelSearch_Click">
                Show all
            </asp:LinkButton>
        </div>
    </div>

    <asp:GridView runat="server" ID="GridSurveys"
        CssClass="table table-bordered table-condensed table-striped table-hover"
        SelectMethod="GridSurveys_GetData" AutoGenerateColumns="false" 
        AllowPaging="true" PagerStyle-CssClass="table-pager"
        >
        <Columns>
            <asp:BoundField HeaderText="Name" DataField="Name" ItemStyle-CssClass="" />
            <asp:HyperLinkField HeaderText="" Text="Compile" DataNavigateUrlFields="Id"
                 DataNavigateUrlFormatString="CompileSurvey?id={0}"/>
        </Columns>
    </asp:GridView>
</asp:Content>
