﻿using Sefin.AskMe.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sefin.AskMe.WebFormApp.Management
{
    public partial class EditSurvey : System.Web.UI.Page
    {
        SurveyServices _services = new SurveyServices();

        SurveyInfo CurrentSurvey {
            get { return ViewState["CurrentSurvey"] as SurveyInfo; }
            set { ViewState["CurrentSurvey"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                InitData();
                PreparePageControls();
            }
        }


        private void InitData()
        {
            var id = Request.QueryString["id"];
            if (!String.IsNullOrEmpty(id))
            {
                CurrentSurvey = _services.GetSurvey(id);
            }
        }

        private void PreparePageControls()
        {
            if (CurrentSurvey == null) {
                LblTitle.Text = "New survey";
            }
            else {
                LblTitle.Text = String.Format("{0} ({1})", CurrentSurvey.Name, CurrentSurvey.Id);
            }
        }

        // The id parameter should match the DataKeyNames value set on the control
        // or be decorated with a value provider attribute, e.g. [QueryString]int id
        public SurveyInfo MainForm_GetItem()
        {
            if (CurrentSurvey != null) return CurrentSurvey;

            return new SurveyInfo();
        }

        // The id parameter name should match the DataKeyNames value set on the control
        public void MainForm_UpdateItem(SurveyInfo survey)
        {
            var newSurvey =  _services.SaveSurvey(survey);
            Response.Redirect("EditSurvey?id="+ newSurvey.Id);
        }

        protected void DrpNumQuestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshQuestionPanels();

        }

        private void RefreshQuestionPanels()
        {
            var drpNumQuestion = (DropDownList)MainForm.Row.FindControl("DrpNumQuestions");
            var val = drpNumQuestion.SelectedValue;

            var panel2 = MainForm.Row.FindControl("PnlQuestion2");
            var panel3 = MainForm.Row.FindControl("PnlQuestion3");

            if (val == "1")
            {
                panel2.Visible = false;
                panel3.Visible = false;
            }
            if (val == "2")
            {
                panel2.Visible = true;
                panel3.Visible = false;
            }
            if (val == "3")
            {
                panel2.Visible = true;
                panel3.Visible = true;
            }
        }

        protected void MainForm_ModeChanged(object sender, EventArgs e)
        {
            RefreshQuestionPanels();
        }

        protected void LnkDelete_Click(object sender, EventArgs e)
        {
            _services.DeleteSurvey(CurrentSurvey.Id);
            
            Response.Redirect("Default");
        }
    }
}