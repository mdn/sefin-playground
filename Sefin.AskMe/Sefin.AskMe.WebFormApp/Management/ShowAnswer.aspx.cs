﻿using Sefin.AskMe.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sefin.AskMe.WebFormApp.Management
{
    public partial class ShowAnswer : System.Web.UI.Page
    {

        SurveyServices _services = new SurveyServices();

        SurveyInfo CurrentSurvey
        {
            get { return ViewState["CurrentSurvey"] as SurveyInfo; }
            set { ViewState["CurrentSurvey"] = value; }
        }

        Answer CurrentAnswer
        {
            get { return ViewState["CurrentAnswer"] as Answer; }
            set { ViewState["CurrentAnswer"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
                PreparePageControls();
            }
        }

        private void InitData()
        {
            var surveyId = Request.QueryString["surveyId"];
            var answerId = Request.QueryString["id"];
            CurrentSurvey = _services.GetSurvey(surveyId);
            CurrentAnswer = _services.GetAnswer(surveyId,answerId);
        }

        private void PreparePageControls()
        {
            LblTitle.Text = String.Format("{0} - {1}", CurrentSurvey.Name, CurrentAnswer.Id);

            TxtName.Text = CurrentAnswer.UserName;
            TxtDate.Text = CurrentAnswer.CompilationDate.ToString("dd/MM/yyyy");

            LblQuestion1.Text = CurrentSurvey.Question1;
            LblAnswer1.Text = SelectText(CurrentAnswer.Answer1, CurrentSurvey.Answer1A, CurrentSurvey.Answer1B, CurrentSurvey.Answer1C);

            PnlQuestion2.Visible = CurrentSurvey.NumberOfQuestions > 1;
            LblQuestion2.Text = CurrentSurvey.Question2;
            LblAnswer2.Text = SelectText(CurrentAnswer.Answer2, CurrentSurvey.Answer2A, CurrentSurvey.Answer2B, CurrentSurvey.Answer2C);

            PnlQuestion3.Visible = CurrentSurvey.NumberOfQuestions > 2;
            LblQuestion3.Text = CurrentSurvey.Question3;
            LblAnswer3.Text = SelectText(CurrentAnswer.Answer3, CurrentSurvey.Answer3A, CurrentSurvey.Answer3B, CurrentSurvey.Answer3C);
        }

        private string SelectText(int? answerNumber, string answerA, string answerB, string answerC)
        {
            if (answerNumber == 1) return answerA;
            if (answerNumber == 2) return answerB;
            if (answerNumber == 3) return answerC;
            return "-";
        }

        protected void LinkGoBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("ShowAnswers?id=" + CurrentSurvey.Id);
        }
    }
}