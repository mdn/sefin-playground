﻿using Sefin.AskMe.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sefin.AskMe.WebFormApp.Management
{
    public partial class ShowAnswers : System.Web.UI.Page
    {

        SurveyServices _services = new SurveyServices();

        SurveyInfo CurrentSurvey
        {
            get { return ViewState["CurrentSurvey"] as SurveyInfo; }
            set { ViewState["CurrentSurvey"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                InitData();
                PreparePageControls();
            }
        }

        private void InitData()
        {
            var id = Request.QueryString["id"];
            if (!String.IsNullOrEmpty(id))
            {
                CurrentSurvey = _services.GetSurvey(id);
            }
        }

        private void PreparePageControls()
        {
            LblTitle.Text = CurrentSurvey.Name;
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        public IQueryable<Answer> GridAnswers_GetData()
        {
            return _services.ListAnswers(CurrentSurvey.Id);
        }
    }
}