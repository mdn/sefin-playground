﻿<%@ Page Title="Answers" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShowAnswers.aspx.cs" Inherits="Sefin.AskMe.WebFormApp.Management.ShowAnswers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="title-management">Answers - <asp:Label runat="server" ID="LblTitle"></asp:Label></h1>


    <div class="panel panel-default">
        <div class="panel-body">
            <asp:HyperLink runat="server" ID="LinkGoBack" NavigateUrl="~/Management/" CssClass="btn btn-default">
                <span class="glyphicon glyphicon-circle-arrow-left"></span> Back to the survey list
            </asp:HyperLink>
        </div>
    </div>

    <asp:GridView runat="server" ID="GridAnswers"
        CssClass="table table-condensed table-striped table-hover"
        SelectMethod="GridAnswers_GetData"
        AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField HeaderText="User" DataField="UserName" />
            <asp:BoundField HeaderText="Compilation date" DataField="CompilationDate" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink runat="server" ToolTip="Show answers"
                        NavigateUrl='<%# String.Format("ShowAnswer?id={0}&surveyId={1}",Eval("Id"),Eval("SurveyId")) %>'>
                        <span class="glyphicon glyphicon-search"></span>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>

    </asp:GridView>
</asp:Content>
