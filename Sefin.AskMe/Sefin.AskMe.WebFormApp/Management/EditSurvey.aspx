﻿<%@ Page Title="Survey Edit" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditSurvey.aspx.cs" Inherits="Sefin.AskMe.WebFormApp.Management.EditSurvey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="title-management">
        <asp:Label runat="server" ID="LblTitle"></asp:Label></h1>


    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:FormView runat="server" DefaultMode="Edit" ID="MainForm"
                SelectMethod="MainForm_GetItem"
                UpdateMethod="MainForm_UpdateItem"
                ItemType="Sefin.AskMe.Logic.SurveyInfo"
                OnDataBound="MainForm_ModeChanged"
                Width="100%">

                <EditItemTemplate>
                    <asp:HiddenField runat="server" ID="TextBox1" Value='<%# Bind("Id") %>' />
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Name</label>
                                <asp:TextBox runat="server" ID="TxtName" CssClass="form-control" Text='<%# Bind("Name") %>' />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="TxtName" ErrorMessage="Required field" />
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <asp:TextBox TextMode="MultiLine" runat="server" ID="TxtDescription"
                                    Text='<%# Bind("Description") %>' CssClass="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Num questions</label>
                                <asp:DropDownList runat="server" ID="DrpNumQuestions" CssClass="form-control"
                                    AutoPostBack="true" SelectedValue='<%# Bind("NumberOfQuestions") %>'
                                    OnSelectedIndexChanged="DrpNumQuestions_SelectedIndexChanged">
                                    <asp:ListItem Value="1" Text="1" />
                                    <asp:ListItem Value="2" Text="2" />
                                    <asp:ListItem Value="3" Text="3" />
                                </asp:DropDownList>
                            </div>

                            <div class="form-group">
                                <label>Active</label>
                                <asp:CheckBox runat="server" ID="ChkActive" CssClass="form-control" Checked='<%# Bind("Active") %>' />
                            </div>
                            <hr />
                            <div class="form-group">
                                <label>Question 1</label>
                                <asp:TextBox runat="server" ID="TxtQuestion1" CssClass="form-control" Text='<%# Bind("Question1") %>' />
                            </div>
                            <div class="form-group">
                                <label>Question 1 - Answer 1</label>
                                <asp:TextBox runat="server" ID="TxtQuestion1Answer1" CssClass="form-control" Text='<%# Bind("Answer1A") %>' />
                            </div>
                            <div class="form-group">
                                <label>Question 1 - Answer 2</label>
                                <asp:TextBox runat="server" ID="TxtQuestion1Answer2" CssClass="form-control" Text='<%# Bind("Answer1B") %>' />
                            </div>
                            <div class="form-group">
                                <label>Question 1 - Answer 3</label>
                                <asp:TextBox runat="server" ID="TxtQuestion1Answer3" CssClass="form-control" Text='<%# Bind("Answer1C") %>' />
                            </div>
                            <asp:Panel runat="server" ID="PnlQuestion2">
                                <hr />
                                <div class="form-group">
                                    <label>Question 2</label>
                                    <asp:TextBox runat="server" ID="TxtQuestion2" CssClass="form-control" Text='<%# Bind("Question2") %>' />
                                </div>
                                <div class="form-group">
                                    <label>Question 2 - Answer 1</label>
                                    <asp:TextBox runat="server" ID="TxtQuestion2Answer1" CssClass="form-control" Text='<%# Bind("Answer2A") %>' />
                                </div>
                                <div class="form-group">
                                    <label>Question 2 - Answer 2</label>
                                    <asp:TextBox runat="server" ID="TxtQuestion2Answer2" CssClass="form-control" Text='<%# Bind("Answer2B") %>' />
                                </div>
                                <div class="form-group">
                                    <label>Question 2 - Answer 3</label>
                                    <asp:TextBox runat="server" ID="TxtQuestion2Answer3" CssClass="form-control" Text='<%# Bind("Answer2C") %>' />
                                </div>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="PnlQuestion3">
                                <hr />
                                <div class="form-group">
                                    <label>Question 3</label>
                                    <asp:TextBox runat="server" ID="TxtQuestion3" CssClass="form-control" Text='<%# Bind("Question3") %>' />
                                </div>
                                <div class="form-group">
                                    <label>Question 3 - Answer 1</label>
                                    <asp:TextBox runat="server" ID="TxtQuestion3Answer1" CssClass="form-control" Text='<%# Bind("Answer3A") %>' />
                                </div>
                                <div class="form-group">
                                    <label>Question 3 - Answer 2</label>
                                    <asp:TextBox runat="server" ID="TxtQuestion3Answer2" CssClass="form-control" Text='<%# Bind("Answer3B") %>' />
                                </div>
                                <div class="form-group">
                                    <label>Question 3 - Answer 3</label>
                                    <asp:TextBox runat="server" ID="TxtQuestion3Answer3" CssClass="form-control" Text='<%# Bind("Answer3C") %>' />
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="panel-footer">
                            <asp:LinkButton runat="server" ID="LnkSave" CssClass="btn btn-primary" CommandName="Update" CausesValidation="true">
                <span class="glyphicon glyphicon-ok"></span> Save
                            </asp:LinkButton>
                            <asp:LinkButton runat="server" ID="LnkDelete" CssClass="btn btn-danger" OnClick="LnkDelete_Click"
                                OnClientClick="return confirm('Are you sure?')">
                <span class="glyphicon glyphicon-delete"></span> Delete
                            </asp:LinkButton>
                            <asp:HyperLink runat="server" ID="LinkGoBack" NavigateUrl="~/Management/" CssClass="btn btn-default">
                <span class="glyphicon glyphicon-circle-arrow-left"></span> Back to the survey list
                            </asp:HyperLink>
                        </div>
                    </div>
                </EditItemTemplate>
            </asp:FormView>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
