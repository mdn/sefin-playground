﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sefin.AskMe.Logic;

namespace Sefin.AskMe.WebFormApp.Survey
{
    public partial class Default : System.Web.UI.Page
    {
        SurveyServices _services = new SurveyServices();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        public IQueryable<SurveyInfo> GridSurveys_GetData()
        {
            var filterText = ViewState["Search"] as string;
            return _services.ListSurveys(filterText).Where(s => s.Active);
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            ViewState["Search"] = TxtSearch.Text;
            GridSurveys.PageIndex = 0;
            GridSurveys.DataBind();
        }

        protected void BtnCancelSearch_Click(object sender, EventArgs e)
        {
            ViewState["Search"] = TxtSearch.Text = String.Empty;
            GridSurveys.PageIndex = 0;
            GridSurveys.DataBind();
        }

    }
}