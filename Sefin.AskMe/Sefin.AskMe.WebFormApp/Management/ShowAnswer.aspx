﻿<%@ Page Title="Survey Answer" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShowAnswer.aspx.cs" Inherits="Sefin.AskMe.WebFormApp.Management.ShowAnswer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="title-management">Answer - <asp:Label runat="server" ID="LblTitle"></asp:Label></h1>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
                <label>Name</label>
                <asp:TextBox runat="server" ID="TxtName" CssClass="form-control" ReadOnly="true"/>
            </div>
            <div class="form-group">
                <label>Date</label>
                <asp:TextBox runat="server" ID="TxtDate" CssClass="form-control" ReadOnly="true" />
            </div>
            <div class="form-group">
                <label><asp:Label runat="server" ID="LblQuestion1"></asp:Label></label>
                <asp:Label runat="server" ID="LblAnswer1" CssClass="form-control" />
            </div>
            <asp:Panel CssClass="form-group" runat="server" id="PnlQuestion2" >
                <label><asp:Label runat="server" ID="LblQuestion2"></asp:Label></label>
                <asp:Label runat="server" ID="LblAnswer2" CssClass="form-control" />
            </asp:Panel>
            <asp:Panel CssClass="form-group" runat="server" id="PnlQuestion3" >
                <label><asp:Label runat="server" ID="LblQuestion3"></asp:Label></label>
                <asp:Label runat="server" ID="LblAnswer3" CssClass="form-control" />
            </asp:Panel>
        </div>
        <div class="panel-footer">
            <asp:LinkButton runat="server" ID="LinkGoBack" OnClick="LinkGoBack_Click" CssClass="btn btn-default">
                <span class="glyphicon glyphicon-circle-arrow-left"></span> Back to the survey
            </asp:LinkButton>
        </div>
    </div>
</asp:Content>
