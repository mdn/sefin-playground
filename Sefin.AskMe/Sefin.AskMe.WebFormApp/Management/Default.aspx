﻿<%@ Page Title="Management" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sefin.AskMe.WebFormApp.Management.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="title-management">Survey Management</h1>
    

    <div class="panel panel-default">
        <div class="panel-body">
            <asp:TextBox runat="server" ID="TxtSearch" CssClass="form-control"  placeholder="Search text"/>
        </div>
        <div class="panel-footer">
            <asp:LinkButton CssClass="btn btn-primary" runat="server" Id="BtnSearch" OnClick="BtnSearch_Click">
                <span class="glyphicon glyphicon-search"></span> Search
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-default" runat="server" Id="BtnCancelSearch" OnClick="BtnCancelSearch_Click">
                Show all
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-warning" runat="server" Id="BtnNew" OnClick="BtnNew_Click">
                <span class="glyphicon glyphicon-plus"></span> New
            </asp:LinkButton>
        </div>
    </div>

    <asp:GridView runat="server" ID="GridSurveys"
        CssClass="table table-bordered table-condensed table-striped table-hover"
        SelectMethod="GridSurveys_GetData" AutoGenerateColumns="false" 
        AllowPaging="true" PagerStyle-CssClass="table-pager"
        >
        <Columns>
            <asp:BoundField HeaderText="ID" DataField="Id" ItemStyle-CssClass="short" />
            <asp:HyperLinkField HeaderText="Name" DataTextField="Name" DataNavigateUrlFormatString="EditSurvey?id={0}" DataNavigateUrlFields="Id" />
            <asp:CheckBoxField HeaderText="Active" DataField="Active" />
            <asp:BoundField HeaderText="Questions" DataField="NumberOfQuestions" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink runat="server" Visible='<%# Eval("Active") %>'  NavigateUrl='<%# "ShowAnswers?id=" +  Eval("Id") %>' ToolTip="Show Answers">
                        <span class="glyphicon glyphicon-th-list"></span>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>
    </asp:GridView>
</asp:Content>
